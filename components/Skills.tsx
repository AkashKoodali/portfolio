import React from 'react'
import Skill from './Skill'
import SkillsBg from './SkillsBG'


type Props  = {
 
}

const Skills = (props: Props) => {

  const skill = [
    {
      image : "https://cdn.pixabay.com/photo/2017/08/05/11/16/logo-2582748_1280.png",
      profi: "95%" ,
      name: "html"
    },
    {
      image : "https://cdn.pixabay.com/photo/2017/08/05/11/16/logo-2582747_1280.png",
      profi: "95%",
      name: "css" 
    },
    {
      image : "https://uploads-ssl.webflow.com/62038ffc9cd2db4558e3c7b7/6242e5dd4337267623f1e7a5_js.svg",
      profi: "92%",
      name: "js"
    },
    {
      image : "https://www.typescripttutorial.net/wp-content/uploads/2020/04/favicon.png",
      profi: "85%",
      name: "ts" 
    },
    {
      image : "https://play-lh.googleusercontent.com/qbeCduZblOk80GaY164lw47gIRjXq9QIzSmgFwqQj1PyhNhTWxYR0OqPzm8BumnmJQ",
      profi: "95%",
      name: "dart" 
    },
    {
      image : "https://miro.medium.com/max/320/0*ObJbOfJnx4QIPUq9.png",
      profi: "95%",
      name: "flutter" 
    },
    {
      image : "https://ionicframework.com/docs/icons/logo-react-icon.png",
      profi: "90%",
      name: "react" 
    },
    {
      image : "https://res.cloudinary.com/startup-grind/image/upload/c_fill,dpr_2.0,f_auto,g_center,h_1080,q_100,w_1080/v1/gcs/platform-data-dsc/events/nextjs-boilerplate-logo.png",
      profi: "95%",
      name: "next" 
    },
    {
      image : "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Tailwind_CSS_Logo.svg/600px-Tailwind_CSS_Logo.svg.png",
      profi: "98%",
      name: "tailwind" 
    },
    {
      image : "https://cdn.icon-icons.com/icons2/2415/PNG/512/mongodb_original_logo_icon_146424.png",
      profi: "95%",
      name: "mongo" 
    },
    {
      image : "https://w7.pngwing.com/pngs/925/447/png-transparent-express-js-node-js-javascript-mongodb-node-js-text-trademark-logo.png",
      profi: "95%",
      name: "express" 
    },
    {
      image : "https://w7.pngwing.com/pngs/780/57/png-transparent-node-js-javascript-database-mongodb-native-miscellaneous-text-trademark.png",
      profi: "95%",
      name: "node" 
    },
    {
      image : "https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/MetaMask_Fox.svg/2048px-MetaMask_Fox.svg.png",
      profi: "60%",
      name: "metamask" 
    },
    {
      image : "https://images.g2crowd.com/uploads/product/image/large_detail/large_detail_96102ac6497377cd53da621075fe828e/sanity.png",
      profi: "60%",
      name: "sanity" 
    },
    {
      image : "https://firebase.google.com/images/brand-guidelines/logo-logomark.png",
      profi: "85%",
      name: "firebase" 
    },
    {
      image : "https://i.etsystatic.com/32954091/r/il/fdd873/3626052920/il_570xN.3626052920_402w.jpg",
      profi: "75%",
      name: "firebase" 
    }
  ]
  
  
  return (
    <div className='flex bg-gray-200 relative flex-col text-center md:text-left pt-20
    xl:flex-row max-w-[2000px] xl:px-10 min-h-screen justify-center xl:space-y-0 mx-auto items-center'>
        <h3 className='absolute top-10 uppercase tracking-[20px] text-gray-500 text-2xl'>Skills</h3>

        <h3 className='absolute top-20 uppercase tracking-[3px] 
        text-gray-500 text-sm'
        >Hover over a skill for currency proficency</h3>

        <div className='grid grid-cols-4 gap-5'>
            {
              skill.map((data, index) => (
                <Skill image={data.image} profi={data.profi} key={index}/>
              ))
            }
        </div>
        <SkillsBg/>
    </div>
  )
}

export default Skills