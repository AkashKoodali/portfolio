import { motion } from "framer-motion";
import { AiFillGithub } from "react-icons/ai";

type Props = {};

const Projects = (props: Props) => {
  const projects = [
    {
      id: 1,
      name: "Netflix Clone",
      image: "project/netflix.jpg",
      desc: "Building Netflix Clone web application with Next JS + React JS + TypeScript + Firebase + Stripe Checkout/Payments + Vercel Hosting + User Authentication (a mobile-friendly)",
      code: "https://github.com/AkashKoodali/netflix-next",
      live: "https://netflix-next-akashkoodali9-gmailcom.vercel.app",
    },
    {
      id: 2,
      name: "Zomato Clone",
      image: "project/zomato.png",
      desc: 
      "This is a Full Stack Zomato Web App Clone. Complete Frontend and Backend is ready. The images are been uploaded and fetched from the AWS s3 Bucket. GoogleOAuth is been implemented. Sign In and Sign Up featues is implemented. Data is been validated using the JOI validation.",
      code: "https://github.com/AkashKoodali/zomato-clone",
      live: "https://master.d2cdoff0wuq43g.amplifyapp.com",
    },
    {
      id: 3,
      name: "Booking.com Clone",
      image: "project/booking.com.jpeg",
      desc: "Full-stack, web application, built using MERN stack, Design and features inspired by Booking.com.",
      code: "https://github.com/AkashKoodali/booking.com-clone",
      live: "",
    },
    {
      id: 4,
      name: "Netflix Mobile",
      image: "project/netflixmob.png",
      desc: "Building Netflix mobile application using Flutter and movieDB Api, using Dio for data fetching, Added Search functionality and using bloc state management with DDD-Architecture. ",
      code: "https://github.com/AkashKoodali/netflix_clone",
      live: "",
    },
    {
      id: 5,
      name: "Amazon mobile clone",
      image: "project/amazon.jpeg",
      desc: "Flutter & NodeJS Full Stack Amazon Clone along with Admin Panel Features Email & Password Authentication, Persisting Auth State, Searching Products Filtering Products (Based on Category), Product Details, Rating, Getting Deal of the Day, Cart, Checking out with Google/Apple Pay, Viewing My Orders, Viewing Order Details & Status",

    //   Sign Out
    //   Admin Panel
    //   Viewing All Products
    //   Adding Products
    //   Deleting Products
    //   Viewing Orders
    //   Changing Order Status
    //   Viewing Total Earnings
    //   Viewing Category Based Earnings (on Graph)",
      code: "https://github.com/AkashKoodali/booking.com-clone",
      live: "",
    },
    // {
    //   id: 6,
    //   name: "Booking.com Clone",
    //   image: "project/booking.com.jpeg",
    //   desc: "Debitis dicta quibusdam labore vitae praesentium fugiat, cumque architecto consectetur expedita",
    //   code: "https://github.com/AkashKoodali/booking.com-clone",
    //   live: "",
    // },
  ];
  return (
    <motion.div
      initial={{
        opacity: 0,
      }}
      transition={{
        duration: 1.5,
      }}
      whileInView={{
        opacity: 1,
      }}
      className="h-screen relative flex overflow-hidden flex-col text-left md:flex-row
    max-w-full justify-evenly mx-auto items-center z-0"
    >
      <h3 className="absolute uppercase tracking-[20px] top-12 text-gray-500 text-2xl">
        Projects
      </h3>

      <div
        className="relative w-full h-fit flex overflow-x-scroll overflow-y-hidden snap-x snap-mandatory z-10
        scrollbar scrollbar-corner-gray-400/20 scrollbar-thumb-[#F7AB0A]/80"
      >
        {/* projects */}
        {projects.map((project) => (
          <div
            key={project.id}
            className="w-screen flex-shrink-0 snap-center flex flex-col space-y-3 items-center
                justify-center p-20 md:p-44 h-screen"
          >
            <motion.img
              //  initial={{
              //     y: -300,
              //     opacity: 0
              //  }}
              //  transition={{
              //       duration: 1.2
              //  }}
              //  whileInView={{
              //      opacity:1,
              //      y: 0
              //  }}
              //  viewport={{
              //      once: true
              //  }}

              src={project.image!}
              alt=""
              className="w-48 h-42 hidden md:block"
            />

            <div className="space-y-5 md:space-y-10 px-0 md:px-10 max-w-6xl">
              <h4 className="text-4xl font-semibold text-center">
                <span className="underline decoration-[#F7AB0A]/50 text-lg">
                  Case Study {project.id} of {projects.length} :
                </span>{" "}
                {project.name}
              </h4>

              <p className="text-lg text-center md:text-left text-gray-400">{project.desc}</p>
              <div className="flex justify-between items-center">
                <a href={project.code} target={"_blank"}>
                  <button className="px-2 py-1.5 bg-black rounded-md flex items-center text-white">
                    Code <AiFillGithub className="h-4 w-4 ml-1" />
                  </button>
                </a>

                <a href={project.live} target={"_blank"}>
                  <button className="px-2 py-1 overflow-hidden bg-white rounded-md text-red-500">
                    Live <span className="text-red-500">.</span>
                  </button>
                </a>
              </div>
            </div>
          </div>
        ))}
      </div>

      <div className="w-full absolute top-[30%] bg-white opacity-[0.03] left-0 h-[500px] -skew-y-12"></div>
    </motion.div>
  );
};

export default Projects;
