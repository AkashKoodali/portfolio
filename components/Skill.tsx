import {motion} from 'framer-motion'

type Props = {
  directionLeft?: boolean;
  image: string;
  profi: string;
}

const Skill = ({directionLeft , image, profi}: Props) => {
  return (
    <>
    <div className='hidden group md:relative md:flex md:cursor-pointer'>
        <motion.img
        initial={{
          x: directionLeft ? -200 : 200,
          opacity: 0
        }}
        transition={{
          duration: 1
        }}
        whileInView={{
          opacity: 1,
          x: 0
        }}
        src= {image}
        className='rounded-full border border-gray-300 object-cover h-20 w-20 filter group-hover:grayscale transition
        duration-300 ease-in-out'
        />

        <div className='md:block absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white
        h-20 w-20 rounded-full z-0'>
          <div className='flex items-center justify-center h-full w-full'>
            <p className='text-2xl font-bold text-black opacity-100'>{profi}</p>
          </div>
        </div>
        
    </div>



    <div className='group relative flex cursor-pointer md:hidden'>
        <img
        src= {image}
        className='rounded-full border border-gray-300 object-cover h-12 w-12 md:h-20 md:w-20 filter group-hover:grayscale transition
        duration-300 ease-in-out'
        />

        <div className='absolute opacity-0 group-hover:opacity-80 transition duration-300 ease-in-out group-hover:bg-white h-12 w-12
        rounded-full z-0'>
          <div className='flex items-center justify-center h-full'>
            <p className='text-sm font-bold text-black opacity-100'>{profi}</p>
          </div>
        </div>
        
    </div>
    </>
  )
}

export default Skill