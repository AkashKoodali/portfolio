import { motion } from "framer-motion";
import AboutBg from "./Aboutbg";
// import Tilt from "react-tilt";
// import { fadeIn, textVariant } from "../utils/motion";



const About = () => {
  return (
    
    <motion.div
      initial={{
        opacity: 0,
      }}
      transition={{
        duration: 1,
      }}
      whileInView={{
        opacity: 1,
      }}
    className="h-screen flex flex-col bg-gray-100 relative text-center md:text-left md:flex-row max-w-full px-10 justify-evenly
    mx-auto items-center"
    >
      <h3 className="absolute top-16 uppercase tracking-[20px] text-gray-500 text-2xl">
        About
      </h3>

      <motion.img
        initial={{
          x: -100,
          opacity: 0,
        }}
        transition={{
          duration: 1,
        }}
        whileInView={{
          x: 0,
          opacity: 1,
        }}
        //    viewport={{
        //     once: true
        //    }}
        //src='https://zomato-clone-1.s3.ap-south-1.amazonaws.com/akash-pic.jpeg'
        src="pic1.jpg"
        className="mb-18 hidden md:block md:mb-0 flex-shrink-0 w-56 h-56 mt-20 rounded-full object-cover md:rounded-lg md:w-64
       md:h-96 z-10"
      />

      {/* <motion.div 
         initial={{
          x: 100,
          opacity: 0,
        }}
        transition={{
          duration: 1,
        }}
        whileInView={{
          x: 0,
          opacity: 1,
        }} 
        */}

       <div 
        className="space-y-10 px-0 md:px-10 text-gray-800"
      >
        <h4 className="text-4xl font-semibold font-serif">
          Here is a{" "}
          <span className="underline decoration-[#F7AB0A]">little</span>{" "}
          background
        </h4>
        <motion.p 
         initial={{
          x: 50,
          opacity: 0,
        }}
        transition={{
          duration: 0.5,
        }}
        whileInView={{
          x: 0,
          opacity: 1,
        }} 
        className="text-sm text-left md:text-lg font-extralight">
          Hi ! I'm a professional Full Stack Developer. I am an expert on MERN
          stack and Next JS. I also have solid foundations in Typescript, HTML5, CSS3,
          Bootstrap, Tailwind and Flutter. I'm happiest when
          I'm creating, learning, exploring and thinking about how to make
          things better. I love to work with people in order to build things
          that matter. I'm eagerly looking for a career in a creative
          organization where I can implement my skills as well as increase my IT
          skills in the software process. Work as part of a cross-functional
          team to provide a comprehensive user experience. The ability to wear
          multiple hats and quickly learn new technologies. The ability to
          test a website and identify bugs and technical problems.
        </motion.p>
        </div>
      {/* </motion.div> */}
      <AboutBg />
    </motion.div>)
    
{/* <>
     <div className='absolute xs:bottom-10 bottom-32 w-full flex justify-center items-center'>
        <a href='#about'>
          <div className='w-[35px] h-[64px] rounded-3xl border-4 border-secondary flex justify-center items-start p-2'>
            <motion.div
              animate={{
                y: [0, 24, 0],
              }}
              transition={{
                duration: 1.5,
                repeat: Infinity,
                repeatType: "loop",
              }}
              className='w-3 h-3 rounded-full bg-secondary mb-1'
            />
          </div>
        </a>
      </div> 

      {/*  */}

  //     <motion.div variants={textVariant()}>
  //       <p className={styles.sectionSubText}>Introduction</p>
  //       <h2 className={styles.sectionHeadText}>Overview.</h2>
  //     </motion.div>

  //     <motion.p
  //       variants={fadeIn("", "", 0.1, 1)}
  //       className='mt-4 text-secondary text-[17px] max-w-3xl leading-[30px]'
  //     >
  //       I'm a skilled software developer with experience in TypeScript and
  //       JavaScript, and expertise in frameworks like React, Node.js, and
  //       Three.js. I'm a quick learner and collaborate closely with clients to
  //       create efficient, scalable, and user-friendly solutions that solve
  //       real-world problems. Let's work together to bring your ideas to life!
  //     </motion.p>

  //     <div className='mt-20 flex flex-wrap gap-10'>
  //       {services.map((service, index) => (
  //         <ServiceCard key={service.title} index={index} {...service} />
  //       ))}
  //     </div>
  //     </> */}
  // );
};

export default About;

// const ServiceCard = ({ index, title , icon }) => (
//   <Tilt className='xs:w-[250px] w-full'>
//     <motion.div
//       variants={fadeIn("right", "spring", index * 0.5, 0.75)}
//       className='w-full green-pink-gradient p-[1px] rounded-[20px] shadow-card'
//     >
//       <div
//         options={{
//           max: 45,
//           scale: 1,
//           speed: 450,
//         }}
//         className='bg-tertiary rounded-[20px] py-5 px-12 min-h-[280px] flex justify-evenly items-center flex-col'
//       >
//         <img
//           src={icon}
//           alt='web-development'
//           className='w-16 h-16 object-contain'
//         />

//         <h3 className='text-white text-[20px] font-bold text-center'>
//           {title}
//         </h3>
//       </div>
//     </motion.div>
//   </Tilt>
// );
