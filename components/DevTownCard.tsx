import { motion } from "framer-motion";

const DevTownCard = () => {
  return (
    <article
      className="flex flex-col rounded-lg items-center space-y-2 md:space-y-5 flex-shrink-0
    w-screen mx-5 h-[400px] md:h-[450px] p-20 md:w-[400px] xl:w-[400px] snap-center bg-gray-900 px-5 hover:opacity-100 opacity-40 justify-center cursor-pointer transition-opacity shadow-lg
    duration-200 overflow-hidden"
    >
      
      <motion.img
        initial={{
          y: -100,
          opacity: 0,
        }}
        transition={{
          duration: 1.2,
        }}
        whileInView={{
          opacity: 1,
          y: 0,
        }}
        // viewport={{
        //     once: true
        // }}
        className="rounded-full h-10 w-10 md:w-[82px] md:h-[82px] object-center mx-auto object-cover"
        src="/devtown.jpeg"
        alt=""
      />

      <div className="px-0 md:px-10">
        <h4 className="text-2xl font-serif font-semibold">
          Devtown Internship
        </h4>
        <p className="font-bold text-1xl mt-1">Full stack web development</p>
        <div className="flex space-x-2 my-2">
          {/* tech used */}
          <img
            src="https://seeklogo.com/images/H/html5-without-wordmark-color-logo-14D252D878-seeklogo.com.png"
            alt=""
            className="h-5 w-5 rounded-full"
          />
          <img
            src="https://e7.pngegg.com/pngimages/602/440/png-clipart-javascript-open-logo-number-js-angle-text.png"
            alt=""
            className="h-5 w-5 rounded-full"
          />

          <img
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Tailwind_CSS_Logo.svg/1200px-Tailwind_CSS_Logo.svg.png"
            alt=""
            className="h-5 w-5 rounded-full object-fill"
          />

          <img
            src="https://www.pngfind.com/pngs/m/685-6854994_react-logo-no-background-hd-png-download.png"
            alt=""
            className="h-5 w-5 rounded-full"
          />

          {/* tech used */}
          <img
            src="https://w7.pngwing.com/pngs/780/57/png-transparent-node-js-javascript-database-mongodb-native-miscellaneous-text-trademark.png"
            alt=""
            className="h-5 w-5 rounded-full"
          />

          <img
            src="https://img.stackshare.io/service/8696/J3fzYcnz_400x400.png"
            alt=""
            className="h-5 w-5 rounded-full"
          />

          <img
            src="https://w7.pngwing.com/pngs/925/447/png-transparent-express-js-node-js-javascript-mongodb-node-js-text-trademark-logo.png"
            alt=""
            className="h-5 w-5 rounded-full"
          />
          <img
            src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTEhIWFhUVFRcWGBgVExgXGBYXFxgWFhUZFxkYHiggGBolHRkYITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy4lICUtLS0tNy0yLy83LS0tLS0tLS0wKzU1NS0tLS0tLi0tLTUtLS0tLS8tLS0vLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAAcBAAAAAAAAAAAAAAAAAQIDBAUGBwj/xABJEAABAwIBBgsFBQYFAgcAAAABAAIDBBEhBQYSMWFxBxMiMkFRUoGRobEUQpLB0QhicoKiFiNDsuHwMzRzk9IVUxclJlRjg8L/xAAZAQEBAAMBAAAAAAAAAAAAAAAABAIDBQH/xAA4EQACAQICBQsCBQQDAAAAAAAAAQIDBBEhBTFxkcEGEjJBQlFhgaGx0SLhFDNSU/ATQ6LxFYKS/9oADAMBAAIRAxEAPwDtaIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCKBcOtQ0x1jxQEyKXTHWPFRBQEUREAREQBERAEREAREQBERAEREAREQBEVI8o6I1DnH5BARdM0a3DxUzHg6jfcreprI4dFp1uNgGi5Nza+5VqizS06rkg+BPyWKnFycU81r8D1xaSeGTKiKk15dzRh1nAd3Wo+zjW838h4fVZHhAzjUMT1AXUbPPugbz8grOqy5BHhpaRHQwX89Sw1TnU88xgaOs8o/IKCtpO1ovCU8/DP2KqdlXqZqOXjkbNxDjrefygD1uoSQsHOPxPP1WkS5Wnfrld3HRHlZWRdfE471zKnKGC6FNva0vbH3LY6Jk+lLcsffA311TTt1vi+JpKkOUqXtM+H+i0RFK+UNbqhH1N60TT/UzexlOl7bPh/op2z051Pi7nNBWgovVyhq9cFva+Q9E0+qTOitgYeaT3OJ+qjxLhqf8Qv6WXO2PIxBIOw2V9BludmqQnY7leqqp8oKb6cGtjT98CeeiZLoyT2pr5N1JeNbb/hPyKNmBwvY9RwPgVr1LnWf4kYO1ht5H6rMUuU4ZsA4XPuuFj3X19y6lDSFtXyhLPueT3PgRVbStSzlHLwzL1FTMBHNdbYcR9QocbbBwtt1jxVpMVUUFFAEREAREQBERAEREBJK+wJ/u/QpJpWxRFzvdFztJ+pwUzsXNG93hq8yFgc8KrmRjp5Z9G/PwUt7c/h6Eqndq29Xyb7al/VqqG/Z1ltkLSnqTK/HR5WwHU0DYPktsljabaQBsbi/QetYbNOHRhL+l7j4DAed1jMs1k8sjoWtcGgkWaDd207PJcy1rKzs41JJylN45a23mvTPgy2vTdxcOEWlGKw2JfcyeUs4mMu2Plu/SO/p7lrVblOWXnvw7IwHh9VkqTNiR2MjgwdXOP0His1TZBgjFyNK3S83HhqU86OkL3p/RHu1ZbNb/wC2C2G2FWztuj9Uu/76txpcUTnGzWlx3E+iv4Mg1Dv4eiOtxA8tfktxbKxoswdzG4fRTcY86mgbz8h9Vto8n6UfzJt7MlxZrnpab6MUtuZrTM1HnnSNG4F30V4zNRnvSOO4Aet1mLPOt9tzR87qHE9bnH8xHoroaIs49jHa2+JNLSFw+1hsSMa3NmD75/MPkFP+zUHU74lf+zt2+JUPZmdkLctHWv7cdyNbu677b3mPdmxB9/4h8wqT81Yuh7xvsfkFlvZ29XmU4gdBcPzFePRto/7cdx6ryuu2zAy5p9mXxZ8wVZzZtTN5oa7c63rZbXoO6HnvAKjpvHZPiPqp56FtJak1sb44m2Oka61tPalwwNCqKCVnPjcNtsPEYK3XRm1HaaRuxHkraaigl1saT1jB3liufW5Pftz8pLivgrp6W/XHd8fc1Kgy3NFhpaTey7Edx1hbNk/LcU3JPJcfddqO46irGrzWbrieRsdiPEavNYOsyXLFzmm3aGI8Rq71qhPSFh0486C815PWvPLwM5RtLrovCW701M3l0BGLPhOru6lBkgOGojWDrWqZKy++OzX3ezzG49O4raYpGTNDmm46CNYPy3Fd2zv6N1HGDz609f3XijmXFrUoP6tXeVkVIOINndx6D9CqqtJgiIgCIiAIiICSP/EP4fU/0Wn5zvvUO+6G+gPzW3l2i8E6iLd98PmsdlXILZnF4cWuNr4XBtgMFzNK21W4oKFPN4p7dZZY1oUqvOnqww9iwyHlyOOIRyXBbfEC4IJJ6Mb4rKftBB2/0O+ixP7Kv/7jfAqpFmp2pfBv1Kho1NKwgoKmsss8OEkV1YWMpOTm8+7/AEy9fnLANRcdzfqQr10oki09EgEaQDhYi2IuO5UaLIsMZuG3d1ux8BqCuKqU83RdbpOiSLdQsutbK5113HZFcW/53s59Z0NVJPa3wKrVFU2zNOF8eo4eqnVZoIoqRqG9oeKmbK06iD3oCdEUrnAazZATIqXtDO0PFTtcDqN0BMiKR0gGsgd6AnUj4gdYUoqG9oeKqAoCmGOHNd3OxHjrUwqLc8W26x49ClM7dV7nqGPonG/cd8P1QFnXZEhlxA0XH3mdO8aisI6iqKV2mzlN6SMQR95vRv8ANbESBiNJh/CbHeNSqsqx73i3EfULn19G0asufH6Z9Tjl6df8zK6V5Ugua/qj3Mo5OrmTsuNzmnWD/eoqvAcMegkeBspIjG2+gAC7E6LcSdth6qeFthjrJJ8TdWUlNQSm031tZY+RNNxcnzVgioiIthiEREAREQECL61TENua5w3HDwKqqCAp02kSSXXbiALDHbdYbOHPegoiRU1LGvH8Nt3ybLsZcjebK4y4+RuT6l0N+NFPOY9DncYGPLNH72lbvXmjIPB3lOtOkyne1pNzJP8Au2m+N+Xyn7wCgOs1nDxQtJEdPUP2kMYDu5RPkqMPD7SE8uknA+66N3qQsFRcAM5b+9rYmO6mROkHi4t9Fb5W4B6tjC6nqYpiPdc0xE7Gm7hfeQNqA6zm1n9k/KB0IZhxh/hSjQf3A4O/KStiFLjru3snHHf1bF4xnhlp5S1wdHLE+xGLXMe0+RBGteoeCXOt2UKEOkN5oXcVKcOUQAWvsO0CL7Q5AVst8I+TaOZ1PPMWyM0dJrYZHAaTQ4YtbbUQe9ZHNvOajygx76WQSBjg112OY5pIuMHAHHoI6j1Fca+0XkfQq4KoDCeMsdh78RGJ3tc0flVt9nnKnF5QkgJ5NRCbDrfGdNv6eMQHoON1gQdbTbu1g+CpSzRxxOnmIa1rTI5ztTGAXPgFGrwcR2wB33t6ELTOHHKfEZJkaDYzvjhG4nTcPhY4d6An/wDGHI//ALl3+xL/AMVt8NRHLE2eEhzXND2Obqc0i/p1rxYvTnAPlPjslNjOuCWSI36iRI3us+35UBvkj8BbW6wHesfl/LtLQRCapkEbC4NB0S5znEE2AaCSbAlXlPi5rexpeXJC4h9o/KulUU1KDhHG6V2+Q6Lb7QGH4kB0vI3CXk2rmZTwzF0khIaDDI0EgF2tzbDAFbUaUX6bdkYC688/Z7yNxuUH1BHJpoyRskluxv6eM8l0Lhsz0fRU7IKdxbPU6Q0wbGONtg4g9DjewPRyjgQEBm86OEbJ9ATHJLpSjXFC0PcD1OxDWHY4grTJuH6nvyKKUjrdKxp8AD6rjmbOb9RX1Ahp26UjgXOLjZrWgjSe89VyNtyF1Sk4AHW/e14DrYhlOXAHe54v4BAbBk3h0ye8gSxTw/eLWvaPhOl+lb7kjK1LWx8bTSslbqLmHlNOuzhradhC4vlLgDqGgmCsikPVJG6LzBfistwKZpV9DW1IqoXRxmDRvpAse/TaW6JabOw091+i6A6/ETiDrBtv6iqipt57+70VRAEREAREQBERAERQc6wuUBR0zGDhdtydYFr49KiKl7ubEd7yGj5nyU0LL8t28DoaPquXZ58NcFO90VFGKh4wMhdaEH7tsZN4sOolAdN4uY63NGxo+ZCq07ZBfTII6LDFebKnhpyq43a+JmxkLSP16RVWj4b8qM53ESfjit/I5qAq/aFye2PKTJGgDjqdjnbXNc+O/wALWDuWa+zVOeMrWdBZC7vaZB/+loGf+ej8qSxSyQtidHHxZ0XEh3KLr4jDXqxW8/Zr/wAxV/6Uf8xQG+8N+R/aMlSOAu+nc2cbm3bJ3aDnH8q885l5U9lr6acmwjmYXH7hOjJ+kuXr+upWyxvieLskY5jh1tcC1w8CvGOVqB1PPLA/nRSPjPRixxbfdggPaEkIcWk+6bjwIx/voXD/ALSeVLvpKYHmtfM4fiIZGf0yeK6tmFlb2rJ1LPe5dC0OP32ciT9TSvOnDLlTj8rVFjdsRbC3ZxYs8fGXoCl+zP8A5F7docr23R0v/h0NC/8AuYLdPs35UtNVUxPPjZK0bWHQf3kPb8K3Y5r/APpv2TR5fsfGW6eO/wAxbfp4LivBDlP2fK1K4mzZHmF23jWljQfzlp7kB6pjgs5zr863dYWXk7hPyr7TlSrkBu0SmNvVoxARC2w6JPevUucmUhTUk9Qf4UL3jaWtJaO82HevHVHTPnmZG3GSWRrBc63vcGi53lAej+AbI3EZNErhZ1TI6XHXoDkMG7kl351oP2j4Hiup3nmOptFv4mSPL/JzV3ekgZS08cTBdsTGRtA12aA0eQutd4Sczm5UpNBpa2Zh04XkYAkYtdbENcLA9RANjayA5DwB5egpqyWOZzWGoY1sb3Gw02uvoEnVpXw2tA1kL0RUxOdbReW22Ag77rxvlvIdRRymKpidG8dDhg4dbXanDaLrM5u8ImUaMBsVS50Y/hy/vGW6hpYtGxpCA9UXlbr5Q8fSx8ip2VLjqDfiPpZcezf4eWmza2lLdV5IHXG/i34gbnHcur5IyrT1sQmppWyMOpzTYtPU4HFpx5pCAvY2WvfWTcqdU4nG5a7WPMdBVRAEREAREQBERAFSnxsOtwHdrKqqnLzmfi9QQgOdcPOcjqahbBG7RfVucwka+KYBxoG/SY3c4rgmaeQ311XFSxkNMjrFxFw1oBc91umzQcOnALrX2lYHWoZLckce0noBPFEDvAd8K0LgfyqynyrA+UhrH6cZcTYAvaQ0/FojvQHfMh8GmTKZgaKSOVwtd87RK5x6+Vg3c0ALJT5l5OfzqCl7qeMHxAus+rXKFdHBG6WZ7WRsF3OcbABAeceHDN2koqqFlJEIhJEXvaHOIJ0yAQHE6OrULDBZr7Nf+Yq/9KP+YrSOE3OkZQrnzsBETWiKK4sSxtzpHaXOcdgIHQtq+zrXNZXzRE2MtOdHa5jmm2/RLj3FAd+hdaZ46CGkbwLH1HgvOfD1kfiMpmUDk1MbZNmm0cW8D4Wu/OvRVTyZA7rsfDB3kR4LnP2hsj8bQMqAOVTSi56o5bMd+vi0BbcAOXW/9NqI5HYUsjpPwxPbp/zNkK4xkendX5Rja4XNTUgvt1SSaUh7gXHuVTNnOJ1LDWxjVVUxht94vaLn/wCsy+IW08AWS+NymJSMKeJ8l+jSdaJo8HuP5UB6W0Ra1sNVti8c5dpHUVfLGy4dT1DtAnqY+8Z8A0r2Dx37zQt7ulfvtZebuH3JfFZUMgGFREyS/RpNvE4b7MafzIDonDXnA05GjMZwrTDax/hkCYndg0fmXNuArI3H5UZIRyKZjpThhpcyMb7u0vyLA5z5yGppMn0+N6WGRjt7pCGgddo2R+K7H9nnI3FUMlSRyqiWzT1xxXaP1mTwCA6fKbvA6hpeOA+aluWnk4js/RU4xpP2OJO8NsArKpy1RRzimkmhZMQHBj3BriDqtfWTbVrQF1XU8FQ3i6iJj2n3JmBw/VhfctHy9wL5OnuYQ+mccf3btJl9rH3w2NIXQHUg90kbNY81JHSFpBD7DpAFgfMoDzDn7wbVWTAJHObNAXaIlYCNEnUJGHmE42sSNt8FLwT5zPosoRWceKne2GVt8CHnRa63W1xBvrtpDpXaeHHK8MOTJYZC0yVGi2Nh1nRex7nW6mgXv12HSvPOaVE6atpomAkvnjGHQNIFx3AXPcgPYNRg5p2keIv8lOpKv3PxfIqdAEREAREQBERAFTnbhhrFiN4xVREBr+fObUeUqJ0BOi42fE+3MkbexOzEtOwleWM4M36milMNTE6NwJsSDovA95jtThtC9h2LTduIOsfMdSp1QilboTRhzTrbIwOb5ghAeVMn8IeU4GCOOtlDALAO0ZLDoALwSBsVWipMq5ZlDQ6eos7F0j3cTF1kk8hmHQMT0Ar0gzNXJbTpCipAR1U8f/FZiORjQGxswGoNbotHoAgOLZz8DYhyYDBeWsiJlkIB/eMtZ0cbfu4EdJsesAaZwUZt1s9bDUUrS1kMrXPmdgwNB5bQffcWkjRHaxsMV6fEzhzm/Cb+N7IKjqY7vsB6oCFW25aOu/orHLeTRV0U1O7+LE+O/U6xAd3Ose5XzWm93HHyG5QxaTYXBxt0g7EB4zqsnTRvdHJE9r2khzS0ggjAhd5+zxkN8VNUVMjHNMz2sZpC12RgnSF+gueR+RdW9qHZd8KldMTg1pG04BAUr8rT+/bu5vriuX/aJyK6Smp6ljC4wyOY/RF7MkANzboDmAfmXVeKGjo7FFs5HOad4xBQHi+loZZHtjjjc57iGta1pJJOoBewMhZNFHRQ07f4UTI79b7AOPe4k96yPtI7LvhUpJcRcWAxsdZKAlibZ7R90j0XEeHPMyskq/bIIXSxOiY13FgucxzbjlNGOjaxuMNd7dPcZGX2EYgqAmcOc2+1v0QHkLJudNfS8mGrnjDcNASO0Rbo0DyfJZV/CdlYixrpLbGxg+Ibdem8o5Oo6j/Hghk/1Ymu/mCxrMyckl2FFSk9QjYfJAeWWNqq2aw46pmdtfK8jaTc2F13rgk4NDQn2urANS5tmMFiIGnnXIwMhGGGAFxc3XSqLJ8UDdGCFkbezGxrB4CwUj3yOw0C1uxzdI+eCAqX0nX6G4DaTrVRURIBgWloGzDyVVrgcQgIoiIAiIgCIiAkmfZpKpvZG0Avdrwu51rnZjZTztu021qlXUjJ49E7welp/voWFRzUW4LF9WORlDm85c7UVTT25jiNhNx9VDjbYOFvQ7itY9qqKR2g7lM6L3sR90+7u8lm6HLkMotfRcfdfqO46io6GkaVSXMn9E+6WW56n5Z+BTVs6kFzo/VHvXwZCyiqZp7cw6OzWPD6KBc4c5ve3Hy1q8kKqKRkoOohToAiIgCIiAIiIAiIgCIiAosZp4k8m9rDptgblUKrKUMGBIB7LRc99tXeq8LdKMtDiDym3GsG5xG3EFY5ubEPSXneR9FJcyuMMKEVj3yery6/5rN9FUW8arfktZbSZ1D3Yid7gPQFUhnYf+0PjP0WSGbtP2T8blY5VzfjbG58dwWi9ibgga9eK5VWnpaMXP8AqR78sOMS6nOwbUVF+f2Zd5OzgZK4MILXHVjcE9V/6LJFtn4anA+It9VpWQWXqIx96/gCfkt2n57dzvkq9EXdS5ouVTWnh6J8TRf0IUaiUO7iToiLqkIREQBERAFScwg3br6R0H+u1VUQFJ7WStLXNv1tOsf31hatlXN58d3R3e3q95vd0jctrkjB2EaiNYUBMW4P+Iau8dCiu7CjdRwms+prWvleDKLe6qUHjHV3Gj0eVZYuY827JxHgdXcs7SZ0tOErCNrcR4HEeav8oZFim5XNcfeb07xqK1ivyJNFjo6Te03HxGsLhTp6QsOhLnRXmt2teWXidSMrS66Swlue/U/5kbZDWQS6nNJ8HeeKuPZrc1xG/EeePmucK7p8oSs5kjhsvceBwW2hyhx/Mh5xfB/JhU0Th0Jb/lfBvehIOyfEKGm7pY7usfQrVYc55xr0XbxY+RCvYs7O1F4O+RC6ENNWctcmtqfDEklo64j1Y7GuOBnePHSCN7Soe0s7QWNZnREdbXjuB+aq/tJB1uH5VQtI2j/uR34e5qdncLsPcX3tLO0PFQ9pb1qy/aKn7R+A/RQdnJB1uP5fqsv+Qtf3Y/8ApGP4Wv8AoluZf8eOgOO5pUS5x1MPeQFiX50xdDHnfYfNWkudjvdiA3uJ8gAtM9LWce2vJN+yNqsLh9nfgbEGPOstG659bIaYa3EnebDyWn1GcUztRDfwj63WPnqnv573O3klRVdP0V+XFvbglxfoUQ0VUfSkl6/C9TdKjK9PELaQ3MF/TBYerzpccImhu12J8BgPNYSlo5JDZjC70G8nALZcnZstbZ0p0j2Rze/pKnp3mkL3KklGPf8Ad6/LM3St7S2zqPnPu+3yYzJsdRPIH6brA3LiSALHEAar7As/nLUaNO4dLyGjvxPkCsjGWjkttybCw6OrAalrGdExkmZCzEt6B2nfQW8VXWpfgbOf1OUp5YvPFvLJbPN4E9Op+JuI5JRjnsSzGZ9LdzpDqaNEbzr8vVbGcXnYAO/WfkqOT6YQRBvULk9bjr+ngq0LbDHWcTvKu0fbfh7eNN69b2v41Et1W/rVXNaurYVERFaThERAEREAREQBERAUuKti022dB7lETkc9veMR9QqiICyqslwy4lov2mYHvtr71hqrNUj/AA5AdjhY+I+i2J0IONrHrGB8kAeNTr/iHzCir6Pt6+c4596ye/5KKV3WpZRll45mkzZGnbricdreV6KxItgRY7cF0Xj3DWz4Tf1soPmYecPibdcupyeg/wAuo1tSfth7F0NLSXSinseHyc6Rb6+lpna2R+ACpOyNSn+G3ue4ehUkuT1fszj54rgyhaWpdcX6Gjot4/6LTdgf7jv+SqtoKZvuR99j6ryPJ+465x/yfBHr0rS6ov0+TQrK6gydK/mxuO3RIHicFvDHRN5oaPws+gVU1B6GO77BVU+T0e3U3LD1xfsaJ6WfZhvf+jVabNeV3PIZ+o+Aw81mKTN2FmJu8/e1eA+d1kNJ56m7sT5qHEA84l2/V4al0qOirWlmo4vxz9NW5EVS+rzy52GzL7+pEStA0WC9uhowHfqCgQ53ONh1N+ZVQBRXRJClTtDXEYC4Fu691ThoI43vl95xJLidQPQOoKu9gOsXUgp29kLCVOMmm1q1eBkpNJpPXrBdpm/ujVtPXuVVEWZiEREAREQBERAEREAREQBERAEREAREQEC0dSl4pvZHgp0QFPiW9keCmDB1BTIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiA/9k="
            alt=""
            className="h-5 w-5 rounded-full"
          />
        </div>
        <p className="uppercase py-2 text-gray-300 font-light font-serif">
          June/2022 - nov/2022
        </p>
        {/* <h1>Devtown are a team that plans to give students a chance to perform research and make projects in the areas of Web
design Development.</h1> */}
<p className="text-sm md:text-lg text-slate-500">Successfully completed 6-months internship as
 Full stack web developer, working on multiple industry level projects.
  </p>
        {/* <ul className="list-disc space-y-4 ml-5 text-md">
          <li>Achievements / Tasks</li>
          working on multiple industry level projects.
          Writing clean code.
          <li>Recommendation letter</li>
        </ul> */}
      </div>
    </article>
  );
};

export default DevTownCard;
