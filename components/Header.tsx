import React from 'react';
import { SocialIcon } from 'react-social-icons';
import {motion} from 'framer-motion'
import {useRouter} from 'next/navigation';


type Props = {}

export default function Header({}: Props){
  const router = useRouter()
  const handleContact= () => {
    router.push('#contact')
  }
  return (
    <header className='sticky top-0 flex bg-transparent items-start justify-between max-w-7xl mx-auto z-20 xl:items-center'>

      <motion.div initial={{
        x: -500,
        opacity: 0,
        scale:0.5
        }
      }
      animate={{
        x:0,
        opacity: 1,
        scale:1
      }} 
      transition={{
        duration: 0.8
      }}
      className='flex flex-row items-center'>
        <SocialIcon url='https://twitter.com/koodaliAkash'
        fgColor='#00acee'
        bgColor='transparent'
        />
        <SocialIcon url='https://github.com/AkashKoodali'
        fgColor='gray'
        bgColor='transparent'
        />
        <SocialIcon url='https://linkedin.com/in/akash-k-049336217'
        fgColor='#0A66C2'
        bgColor='transparent'
        />
        <SocialIcon url='https://www.facebook.com/akash.aku.543'
        fgColor='#3b5998'
        bgColor='transparent'
        />
        <SocialIcon url='https://www.instagram.com/akash_koodali/'
        fgColor='#E1306C'
        bgColor='transparent'
        />
      </motion.div>

  
      <motion.div
      initial={{
        x: 500,
        opacity: 0,
        scale:0.5
        }
      }
      animate={{
        x:0,
        opacity: 1,
        scale:1
      }} 
      transition={{
        duration: 0.9
      }}
      className='flex flex-row items-center text-gray-300 cursor-pointer'
      onClick={handleContact}>
      <SocialIcon 
        className='cursor-pointer'
        network='email'
        fgColor='red'
        bgColor='transparent'
        />
        <p className='uppercase hidden md:inline-flex text-sm text-gray-400'>Get in touch</p>
        
      </motion.div>
     
      {/* <SoundBar/> */}
  
      
    </header>
  )
}

