import { motion } from "framer-motion";

type Props = {};

const AvodhaExperienceCard = (props: Props) => {
  return (
    <article
      className="flex flex-col rounded-lg items-center space-y-2 md:space-y-5 flex-shrink-0
      w-screen mx-5 h-[400px] md:h-[450px] p-20 md:w-[400px] xl:w-[400px] snap-center bg-gray-900 px-5 hover:opacity-100 opacity-40 justify-center cursor-pointer transition-opacity shadow-lg
      duration-200 overflow-hidden"
    >
      <motion.img
        initial={{
          y: -100,
          opacity: 0,
        }}
        transition={{
          duration: 1.2,
        }}
        whileInView={{
          opacity: 1,
          y: 0,
        }}
        // viewport={{
        //   once: true,
        // }}
        className="rounded-full h-10 w-10 md:w-[82px] md:h-[82px] object-center mx-auto object-cover"
        src="/avodha.jpeg"
        alt="profile"
      />

      <div className="px-0 md:px-10">
        <h4 className="text-2xl font-serif">Avodha Skill Training</h4>
        <p className="font-bold text-1xl mt-1">Flutter development</p>
        <div className="flex space-x-2 my-2">
          {/* tech used */}
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/7/7e/Dart-logo.png"
            alt=""
            className="h-5 w-5 rounded-full"
          />

          <img
            src="https://miro.medium.com/max/320/0*ObJbOfJnx4QIPUq9.png"
            alt=""
            className="h-5 w-5 rounded-full object-fill"
          />

          <img
            src="https://miro.medium.com/max/300/1*R4c8lHBHuH5qyqOtZb3h-w.png"
            alt=""
            className="h-5 w-6 rounded-full"
          />

         
        </div>
        <p className="uppercase py-2 text-gray-300 font-light font-serif">June/2022 - nov/2022</p>
        {/* <ul className="list-disc space-y-4 ml-5 text-md">
          <li>Worked as a mobile developer.</li>
          <li>Worked on different projects.</li>
        </ul> */}
        <p className="text-sm md:text-lg text-slate-500">Successfully completed skill training in Flutter.
  </p>
      </div>
    </article>
  );
};

export default AvodhaExperienceCard;
