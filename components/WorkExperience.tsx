import {motion} from 'framer-motion'
import AvodhaExperienceCard from './AvodhaEx'
import DevTownCard from './DevTownCard'

type Props = {}

const WorkExperience = (props: Props) => {
  return (
    <motion.div 
    initial={{
      opacity: 0,
     }}
     transition={{
      duration: 1.5
     }}
     whileInView={{
      opacity: 1
     }}
    className='h-screen flex relative overflow-hidden flex-col text-left md:flex-row
    px-10 justify-center mx-auto items-center'>
      <h3 className='absolute top-16 uppercase tracking-[20px] text-gray-500 text-2xl'>Experience</h3>

      <div className='w-screen flex justify-start md:justify-center space-x-5 mt-10 p-10 snap-x snap-mandatory 
      overflow-x-scroll scrollbar mx-2 
      scrollbar-corner-gray-400/20 scrollbar-thumb-[#F7AB0A]/80'>
        {/* Experience card */}
        <DevTownCard/>
         <AvodhaExperienceCard/> 
      </div>
    </motion.div>
  )
}

export default WorkExperience