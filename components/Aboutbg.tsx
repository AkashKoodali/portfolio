import { BsQuestionLg } from "react-icons/bs";

const AboutBg = () => {
  return (
    <>
      <BsQuestionLg className="absolute text-7xl opacity-10 top-[25%] left-[12%] text-gray-900" />
      <BsQuestionLg className="absolute text-xl opacity-10 bottom-[25%] left-[16%] text-gray-900" />
      <BsQuestionLg className="absolute text-lg opacity-10 top-[25%] left-[60%] text-gray-900" />
      <BsQuestionLg className="absolute text-9xl opacity-10 top-[13%] left-[30%] text-gray-900" />
      <BsQuestionLg className="absolute text-md opacity-10 top-[48%] right-[13%] text-gray-900" />
      <BsQuestionLg className="absolute text-3xl opacity-10 bottom-[15%] left-[20%]text-gray-900" />
      <BsQuestionLg className="absolute text-8xl opacity-10 top-[80%] right-[30%]text-gray-900 text-gray-900" />
      <BsQuestionLg className="hidden md:inline absolute text-2xl opacity-10 top-[15%] right-[14%]text-gray-900 text-gray-900" />
      <BsQuestionLg className="absolute text-4xl opacity-10 top-[67%] left-[25%] text-gray-900" />
      <BsQuestionLg className="absolute text-5xl opacity-10 top-[34%] right-[7%] text-gray-900" />
    </>
  );
};

export default AboutBg;