import Link from "next/link";
// import BackgroundCircles from "./BackgroundCircles";
import { Cursor, useTypewriter } from "react-simple-typewriter";
import HeroBg from "./HeroBg";
//import HeroCircles from "./HeroCircles";
import {motion as m} from 'framer-motion'


type Props = {};

export default function Hero({}: Props) {

  const [text, count] = useTypewriter({
    words: [
      // "<ButLovesToCodeMore />"
      "Hello, I am Akash ",
      "I-am-web-developer",
      "and-Web Designer", 
    ],
  //  deleteSpeed: 30,
    loop: true,
    delaySpeed: 2000,
  });

  const item = {
    hidden: { opacity: 0, y: 100 },
    visible: { opacity: 1, y: 0 },
  };
  
  return (
    <div className="h-screen relative mx-auto overflow-y-hidden bg-black flex flex-col space-y-2 items-center justify-center">

      {/* <BackgroundCircles /> */}
      {/* <HeroCircles/> */}
      <img src="/JM9R.gif" alt="" className="absolute h-[320px] w-[320px] sm:h-[525px] sm:w-[525px]" />
      {/* <img
        className="rounded-full h-32 w-32 mx-auto object-covers"
        src="/akash.jpg"
        //src="https://zomato-clone-1.s3.ap-south-1.amazonaws.com/akash-pic.jpeg"
        alt="profile"
      /> */}

      <h2 className="text-sm uppercase text-gray-500 pb-2 tracking-[15px] z-20  ">
        FULL STACK DEVELOPER
      </h2>
     
      <div className="relative z-20 my-5">
        <h1 className="text-5xl lg:text-6xl font-semibold px-10 z-20">
          <span className="mr-3">{text}</span>
          <Cursor cursorColor="red" />
        </h1>
      </div>
      <div className="pt-5 z-20">
        <Link href='#about'>
          <button className="heroButton">About</button>
        </Link>
        <Link href='#experiance'>
          <button className="heroButton">Experience</button>
        </Link>
        <Link href='#skills'>
          <button className="heroButton">Skills</button>
        </Link>
        <Link href='#projects'>
          <button className="heroButton">Projects</button>
        </Link>
        
      </div>
      <HeroBg/>

      <div className="absolute bottom-28 sm:bottom-4 left-[50%] -translate-x-1/2 flex justify-center ">
        <m.div
          variants={item}
          className="border-4 border-white rounded-full flex justify-center items-center h-12 w-6"
        >
          <m.div
            animate={{ y: ["100%", "-100%"] }}
            transition={{
              repeat: Infinity,
              repeatType: "reverse",
              duration: 1.2,
            }}
            className="bg-white h-3 w-3 rounded-full"
          ></m.div>
        </m.div>
      </div>
    </div>
    
    
  );
}
