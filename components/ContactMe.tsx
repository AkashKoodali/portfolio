import { PhoneIcon, MapIcon, EnvelopeIcon } from '@heroicons/react/24/solid';
import { useForm, SubmitHandler } from 'react-hook-form'

type Inputs = {
    name: string,
    email: string
    subject: string,
    message: string
}

type Props = {};

const ContactMe = (props: Props) => {
    const { register, handleSubmit } = useForm<Inputs>();
    const onSubmit: SubmitHandler<Inputs> = (formData) => 
    window.location.href = `mailto:akashkoodali9@gmail?subject=${formData.subject}&body=Hi, my name is ${formData.name}.
    ${formData.message} (${formData.email})`;
    
  return (
    <div className='h-screen bg-gray-100 flex relative flex-col text-center md:text-left md:flex-row
     px-10 justify-evenly mx-auto items-center'>
        {/* bg-[rgb(36,36,36)] */}
        <h3 className='absolute top-10 uppercase tracking-[20px] text-gray-500 text-2xl'>Contact</h3>

        <div className='flex flex-col space-y-8 text-gray-900'>
            {/* <h4 className='text-2xl font-serif font-light text-center'>I have got just what you need.{" "}<span className='decoration-[#F7AB0A]/50 underline'>
            Let's Talk</span>
            </h4> */}

            <div className='space-y-5'>

                <div className='flex flex-col text-left w-full justify-center items-center mb-10'>

                <div className='flex items-center space-x-3 '>
                    <PhoneIcon className='text-[#F7AB0A] h-4 w-4 animate-pulse'/>
                    <p className='text-md font-light'>+91{" "}8921558933</p>
                </div>

                <div className='flex items-center space-x-3 '>
                    <EnvelopeIcon className='text-[#F7AB0A] h-4 w-4 animate-pulse'/>
                    <p className='text-md font-light'>akashkoodali9@gmail.com</p>
                </div>
                
                <div className='flex items-center space-x-3 '>
                    <MapIcon className='text-[#F7AB0A] h-4 w-4 animate-pulse'/>
                    <p className='text-md font-light'>Based In Kannur, Kerala</p>
                </div>

                </div>
                


                <form onSubmit={handleSubmit(onSubmit)} className='flex flex-col space-y-2 w-screen md:w-fit mx-auto bg-gray-100 border-2 border-gray-300 p-10 '>
                    <div className='flex flex-col md:flex-row space-x-2'>
                        <input { ...register('name')} placeholder='Name' className='contactInput' type="text" />
                        <input { ...register('email')} placeholder='Email' className='contactInput' type="email" />
                    </div>

                    <input { ...register('subject')} placeholder='Subject' className='contactInput' type="text" />

                    <textarea {...register('message')} placeholder='Message' className='contactInput'/>
                    <button  className='bg-gray-900 py-5 px-10 rounded-full text-white font-bold'>Submit</button>
                </form>
            </div>
        </div>
    </div>
  )
}

export default ContactMe