import type { NextPage } from 'next'
import Header from '../components/Header'
import Head from 'next/head'
import Hero from '../components/Hero'
import About from '../components/About'
import WorkExperience from '../components/WorkExperience'
import Skills from '../components/Skills'
import Projects from '../components/Projects'
import ContactMe from '../components/ContactMe'

const Home: NextPage = () => {
  return (
    <div className='bg-[#121212] text-white h-screen snap-y snap-mandatory overflow-y-scroll overflow-x-hidden 
    first-letter first-letter z-0 scrollbar scrollbar-corner-gray-400/20 scrollbar-thumb-[#F7AB0A]/80'>
      <Head>
        <title>Akash's Portfolio</title>
        <link
          rel="icon"
          href="https://w0.peakpx.com/wallpaper/989/1006/HD-wallpaper-ak-alphabet-comics-letter-name.jpg"
        />
      </Head>

      
      {/* <Welcome/> */}

      {/* Heder */} 
      <Header/>
     

      {/* Hero */}
      
      <section id='hero' className='snap-start'>
      {/* <Homee /> */}
        <Hero/>
      </section>

      {/* About */}
      <section id='about' className='snap-center'>
      <About />
      </section>

      {/* Experiance */}
      <section id='experiance' className='snap-center'>
      <WorkExperience/>
      </section>
      

      {/* Skills */}
      <section id='skills' className='snap-start'>
        <Skills/>
      </section>

      {/* Projects */}
      <section id='projects' className='snap-start'>
        <Projects/>
      </section>

      {/* Contact me */}
      <section id='contact' className='snap-start'>
        <ContactMe/>
      </section>

    </div>
  )
}

export default Home
